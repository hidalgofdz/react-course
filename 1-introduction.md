---
nocite: |
	@JavaScriptHistoryHow2014
...

# Introducción

Antes de aprender sobre cualquier tecnología, metodología o herramienta es importante entender qué es, para que sirve, quien la creó y por qué. 

## ¿Qué es React?
React es una librería para construir interfaces de usuario en la web 
(o móvil) que utiliza una arquitectura basada en componentes. En React cada componente contiene todo lo necesario para funcionar y renderizarse por sí mismo, con la intención de disminuir el acoplamiento y mejorar la reusabilidad.

Si hablamos de una arquitectura en capas Modelo Vista Controlador (MVC), React representaría solo la capa de Vista. 

React **NO** es responsable de:

- Eventos del sistema(como clics, hovers, o presionar enter).
- Manejo de peticiones AJAX.
- Manejo o persistencia de datos.

Es responsabilidad del desarrollador elegir las herramientas adecuadas para el manejo de lo anterior.

React **SI** es responsable de:

- Manejar el estado de las vista.
- Asegurar que se renderice la información adecuada en la vista. 

## ¿Quién es el creador de React?
El autor original de la librería es Jordan Walke y actualmente es mantenida por Facebook, Instagram y una comunidad de desarrolladores.

## ¿Por qué nació React?
Cambiar el DOM dependiendo del estado (o datos) de la aplicación es una operación que puede tomar demasiado tiempo. Es por ello que aplicaciones de tamaño mediano o grandes que manipulan el DOM directamente pueden comprometer su desempeño.  

Librerías como jQuery al manipular el DOM remplazan completamente un nodo y el navegador lo re-construye cada vez que hay un cambio.  El desempeño de la aplicación se ve comprometido cuando un nodo tiene muchos hijos o se deben re-construir muchos nodos a la vez.

Para tratar de solucionar lo anterior _React_ hace uso de un _DOM virtual_  y un algoritmo de comparación.  Cada vez la interfaz de usuario debe de mostrar un cambio el algoritmo compara la versión anterior del _DOM virtual_ con la versión actual, con el objetivo de detectar que partes del DOM son diferentes y solo actualizar esos nodos (en vez de reconstruirlo por completo). Debido a que el algoritmo de comparación tiene una complejidad O(n) y solo se remplazan los nodos necesarios. _React_ es capaz de actualizar la interfaz de usuario de una forma rápida y eficiente.

## ¿Por qué utilizar React?
React se ha posicionado como una de las primeras librerías para crear aplicaciones web debido a su corta curva de aprendizaje. Para utilizarla solo debes de comprender qué es state y props, y cúal es el ciclo de vida de un componente.

Los beneficios de usar React son:

- Su corta curva de aprendizaje,
- La capacidad de reutilizar componentes a través de la aplicación,
- El uso de un DOM virtual que permite a las aplicaciones renderizar rápidamente y,
- Una abstracción limpia para trabajar.  


