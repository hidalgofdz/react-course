
# ES6 Crash course
React puede hacer uso del nuevo estándar de Javascript ES6 que permite realizar acciones más rápidamente y mejorar la legibilidad del código.

## let” y “const” en vez de “var”
Antes para definir una variable en JS se hacía uso `var` pero a partir de ES6 existen otras dos maneras de definir variables, `let` y `const`. 

`let`  es para definir variables el cual su valor puede cambiar en el futuro.

```js
let num = 4;
console.log(num); // imprime: 4
num = 5;
console.log(num); // imprime: 5
```

`const`  es para definir variables el cual su valor no puede cambiar.

```
const num = 4;
console.log(num); // imprime: 4
num = 5; // Lanza una excepción: TypeError: Assigment to constant variable. 
```

Aún puede hacerse uso de var pero no es recomendado debido al var hoisting @varJavaScriptMDN

## Arrow functions
Esta es una nueva manera de definir funciones en JS que además de traer consigo una nueva sintaxis también trae cambios en cómo utilizar el atributo this. 

Primero veamos los cambios en la sintaxis. 

Imagina que tenemos un siguiente array de números y queremos imprimir cada uno de ellos en consola.

```js
const numbersList = [ 1, 3, 4, 6 ];

numbersList.forEach(function (number){
 console.log(number);
});

// Esto imprimirá en consola:  1 3 4 6
```

Utilizando la sintaxis de arrow functions el código quedaría de la siguiente manera:

```js
numbersList.forEach((number) => {
 console.log(number);
});
// Esto imprimirá en consola:  1 3 4 6
```


La función dentro de `forEach` solo tiene un atributo por lo que la arrow function se puede simplificar más: 

```js
numbersList.forEach(number => {
 console.log(number);
});
// Esto imprimirá en consola:  1 3 4 6
```

Y por qué la arrow function solo hace una operación se puede simplificar aún más: 

```js
numbersList.forEach(number => console.log(number));
// Esto imprimirá en consola:  1 3 4 6
```

### Arrow functions y el contexto de this

Analizando el siguiente código:

```js
class Task {
  constructor(name) {
    this.name = name;
  }
};

class TodoList {
  constructor(tasks, name){
    this.name = name;
    this.tasks = tasks;
  }

  printTasks(){
    this.tasks.forEach(function(task) {
    	console.log('Task name: '+ task.name);
    });
  }
}

const taskList = [new Task('task 1'), new Task('task 2')];
const todoList = new TodoList(taskList, "Today's tasks");

console.log(todoList.name);
todoList.printTasks();
```

## Parámetros por defecto
## Rest
## Spread
## Object shorthand
## Object methods
## Object restructuring
## Classes
## ES6 Modules
## Promises
## Arrays
### forEach
### map
La función map, itera por todo el array; por cada elemento ejecuta la función callback pasando al elemento como atributo y retorna un nuevo array con los resultados de cada función callback como resultado. 

### filter
Este método itera por un array y da como resultado un nuevo array con los elementos que cumplan la condición especificada en la función callback.

Por ejemplo, si tenemos una lista de ingredientes del cual queremos sacar aquellos cuyo nombre empiece con la letra ‘b’ el método sería:

```js
const ingredients = ['eggs','bums','meat','bbq sauce'];
const filteredIngredients = ingredients.filter(ingredient => {
	return ingredient[0] === 'b'
});

console.log(filteredIngredients); // ['bums', 'bbq sauce']
```


### reduce
### includes
### find

Para conocer más sobre las mejores que incorpora ES6 recomiendo el ES2015 Crash Course de laracast @ES2015CrashCourse.
