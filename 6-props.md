# React Props

Los props son la manera en cómo pasar información entre un componente padre y sus hijos. Los props son para los componentes lo que los argumentos son para las funciones. 

```js
class HelloUser extends React.Component {
  render() {
    return (
      <div> Hello, {this.props.name}</div>
    )
  }
}
ReactDOM.render(<HelloUser name="Tyler"/>, document.getElementById('app'));
```

En el ejemplo anterior el atributo `name` es pasado a través de props al componente HelloUser y este atributo puede ser accedido dentro del componente vía `this.props.name`  

Los props pueden ser datos de casi cualquier tipo:  integer, floats, objects, funciones, Componentes de React, etc.

## Ejemplos

### Usar Map para crear una lista de componentes hijos

Hagamos un componente para listar los ingredientes de una receta: 

```js
class Recipe extends React.Component {
  render() {
    var name = 'Buevito with capsun';
    var ingredients = ['buevito', 'capsun', 'love'];
    return (
      <div>
        <h3> Name: {name} </h3>
        <IngredientList ingredients={ingredients} />
      </div>
    )
  }
}
```

El componente ```IngredientsList.jsx``` se vería de la siguiente manera: 

```js
class IngredientList extends React.Component {
  render() {
    return (
      <div>
        <h3> Ingredients </h3>
        <ul>
          {this.props.ingredients.map((ingredient) => {
            return <li> {ingredient} </li>;
          }}
        </ul>
      </div>
    )
  }
}
```