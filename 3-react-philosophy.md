
# La Filosofía de React
Para sacar el mayor provecho a React (o a cualquier librería o framework) uno debe adecuarse a su filosofía trabajo. Aunque la curva de aprendizaje es pequeña puede llegar a ser diferente a lo que como desarrolladores estamos acostumbrados al realizar aplicaciones web.  A continuación se presentan algunas consideraciones que uno debe tener al utilizar React.

## Composición
En React todo está definido en términos de componentes. Un componente debe ser responsable de una sola funcionalidad y contener dentro de sí lo necesario para realizarla.

- `<Card />`
- `<Header />`
- `<Router />`
- `<Carousel />`

En la lista anterior cada tag representa a un solo componente en React, por ejemplo el componente \<Carousel /\> puede renderizar algo como lo siguiente: 

<p align="center">
![El componente \<Carousel /\> al renderizarse.][image-1]
</p>

Y contener la lógica necesaria para:

- Mostrar las imágenes 
- Cambiar de imagen cuando el usuario haga clic en alguna flecha
- Mostrar en los indicadores que imagen se está desplegando
- Etc.   

Al encapsular todo lo necesario para renderizar un carrousel, el desarrollador que utilice el componente no debe de preocuparse por como internamente funciona.

Un ejemplo más complejo puede verse en un post de instagram: 

<p align="center">
![][image-2]
</p>


Cada elemento que está dentro de un cuadro rojo representa un componente. Los componentes pueden estar compuestos de otros componentes.  En el ejemplo anterior podemos ver como cada componente solo tiene una responsabilidad: 

- Mostrar una imagen
- Mostrar una lista de comentarios
- Permitir al usuario poner “me gusta” al post. 
- Escribir un comentario
- Ordenar la posición de otros componentes
- Etc.   


[image-1]:	file:///Users/hidalgofdz/Documents/react-guide/images/carousel-example.png "Ejemplo Carousel"
[image-2]:	file:///Users/hidalgofdz/Documents/react-guide/images/instagram-components-example.png "Ejemplo de componentes en Instagram"