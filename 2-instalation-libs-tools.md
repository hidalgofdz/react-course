# Instalación, Librerías y Herramientas

Para poder crear aplicaciones con React se debe tener instalados las siguientes librerías y herramientas:

## Librerías:

### Node(mínimo versión 6)
Node (versión 6) es un entorno de ejecución que nos servirá como servidor para nuestra aplicación React.

### create\_react\_app

create_react_app es una librería que utiliza webpack para automatizar la construcción de un entorno de desarrollo para una aplicación con React y evita la necesidad de tener que configurar todo lo necesario de forma manual. create_react_app construye un entorno de desarrollo con: 

- Soporte para React, JSX, ES6, y la sintaxis Flow
- Un servidor de desarrollo con hot-reloading.
- La capacidad de importar CSS e Imágenes directamente a JS.
- entre otras cosas.  


Aunque la librería create\_react\_app no es necesaria para construir una aplicación con React, se usará durante el curso para evitar construir manualmente nuestro ambiente de desarrollo. 

## Herramientas

- Editor o IDE(sublime, atom, webstorm, vscode, notepad)
- Terminal  

## Instalación (en terminal)

```bash
npm install -g create-react-app // Instala globalmente create_react_app

create-react-app my-app // Crear una nueva react app en el directorio
                        // “my-app”
cd my-app/ // Ir al directorio “my-app”
npm start  // Iniciar el servidor node con la app de react
```

`npm start` levantará un servidor con la aplicación en la dirección `localhost:3000`
