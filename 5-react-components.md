# Componentes en React

Los componentes son los bloques de construcción en React, en ellos se encuentra lo necesario para renderizarse y manipular su estado. Para definir que debe renderizar un componente se hace uso de la sintaxis JSX, aunque también es posible definir los componentes de usando JS puro. En general los componentes en React solo utilizan javascript y JSX para definir componentes. 

JSX es el formato estándar para crear crear componentes en React, pero para que el navegador pueda interpretarlo es necesario compilarlo a JS. Ahí es donde aparece Babel, que es una librería encargada de compilar los componentes por nosotros. Babel viene descargado y configurado por defecto al crear una aplicación con create\_react\_app.

Un componente sencillo tiene la siguiente estructura: 

`archivo: index.html`
```html
<!DOCTYPE html>
<html lang="en">
  <body>
    <div id='app'></div>
  </body>
</html>
```

`archivo App.jsx`
```js
import React, {Component} from 'react';
import ReactDom from 'react-dom'

class HelloWorld extends React.Component {
  render() {
    return (
      <div>Hello World!</div>
    )
  }
}

ReactDOM.render(<HelloWorld />, document.getElementById('app'));
```

Donde el resultado del método `render` es el HTML que será renderizado al DOM. 

En ejemplo anterior ocurre lo siguiente: 

1. `ReactDom` le dice al componente \<HelloWorld /\> que se renderice dentro del elemento del DOM que tenga el id “app”.
2. El componente `<HelloWorld />` renderiza el contenido que tiene dentro de su método `render()`.
3. Se muestra en pantalla el contenido `<div>Hello World!</div>`.

`ReactDOM.render` es el método encargado de renderizar los componentes de React en el DOM y toma dos argumentos, el primero es el componente a renderizar y el segundo es el nodo DOM en donde se debe renderizar.

Normalmente el método `ReactDOM.render` se utiliza una sola vez en el componente padre más alto dentro de la aplicación. 


